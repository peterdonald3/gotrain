package main

import (
	"encoding/json"
	"net/http"

	gks "gitlab.com/peterdonald3/gokeystore"
)

/*
Description:

	Handles DELETE request.
	Deletes key from store if exists.
	Returns notification if found or not found.
*/
func deleteKeys(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	defer r.Body.Close()

	var (
		rKeys *gks.KeyRequest
	)
	err := json.NewDecoder(r.Body).Decode(&rKeys)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	rKeys = rKeys.DeleteKeys()

	json.NewEncoder(w).Encode(rKeys)
}
