package main

import (
	"encoding/json"
	"net/http"

	gks "gitlab.com/peterdonald3/gokeystore"
)

/*
Description:

	Handles POST request.
	creates key in store if does not exist already.
	Returns notification explaining if value exists or already existed.
*/
func postKeys(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	defer r.Body.Close()

	var (
		rKeys *gks.KeyRequest
	)
	err := json.NewDecoder(r.Body).Decode(&rKeys)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	rKeys = rKeys.CreateKeys()

	json.NewEncoder(w).Encode(rKeys)
}
