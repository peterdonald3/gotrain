package main

import (
	"log"
	"net/http"
)

/*
Description:

	Handles all requests.
	sends to functions if POST/GET/PUT/DELETE
	returns error if not required method or content-type is not application/json
*/
func requestHandler() {
	http.HandleFunc("/keys", func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Content-Type") == "application/json" {
			switch r.Method {
			case http.MethodPost:
				postKeys(w, r)
			case http.MethodGet:
				getKeys(w, r)
			case http.MethodPut:
				putKeys(w, r)
			case http.MethodDelete:
				deleteKeys(w, r)
			default:
				http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
			}
		}

		if r.Header.Get("Content-Type") != "application/json" {
			http.Error(w, "request must be application/json", http.StatusMethodNotAllowed)
		}

	})
	log.Fatal(http.ListenAndServe(":8888", nil))
}
