FROM golang:latest AS builder
 
RUN mkdir -p /go/src/gitlab.com/peterdonald3/gotrain
COPY . /go/src/gitlab.com/peterdonald3/gotrain
WORKDIR /go/src/gitlab.com/peterdonald3/gotrain

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o go-ksrestapi

FROM alpine:latest
RUN apk --no-cache add libc6-compat ca-certificates

COPY --from=builder /go/src/gitlab.com/peterdonald3/gotrain/go-ksrestapi  ./

EXPOSE 4444

ENTRYPOINT ["/go-ksrestapi"]