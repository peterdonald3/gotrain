package main

import (
	"encoding/json"
	"net/http"

	gks "gitlab.com/peterdonald3/gokeystore"
)

/*
Description:

	Handles GET request.
	Reads key from store for those requested.
	Returns all keys if none specific request.
	Returns notification if no keys are in store at time of request.
*/
func getKeys(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	defer r.Body.Close()

	var (
		rKeys *gks.KeyRequest
	)
	err := json.NewDecoder(r.Body).Decode(&rKeys)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	rKeys = rKeys.ReadKeys()

	json.NewEncoder(w).Encode(rKeys)
}
