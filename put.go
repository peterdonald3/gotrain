package main

import (
	"encoding/json"
	"net/http"

	gks "gitlab.com/peterdonald3/gokeystore"
)

/*
Description:

	Handles PUT request.
	updates keyStore with updated values.
	If autocreate enabled then adds request to keyStore.
	Returns notification explaining if value does not exist.
*/
func putKeys(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json")

	defer r.Body.Close()
	var (
		rKeys *gks.KeyRequest
	)
	err := json.NewDecoder(r.Body).Decode(&rKeys)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	rKeys = rKeys.UpdateKeys()

	json.NewEncoder(w).Encode(rKeys)
}
