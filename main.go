package main

/*
Description:

	Initialises keyStore so value is not nil.
	Saves having to if statement for if keyStore == nil.
*/
func main() {
	requestHandler()
}
